import React, { useEffect, useState } from 'react';
import { Progress } from 'antd-mobile';
import Footer from './components/Footer/Footer';
import { Route, Switch, Redirect, useLocation } from 'react-router-dom'
import Home from './pages/Home/Home';
import Category from './pages/Category/Category';
import Main from './pages/Main/Main';
import Detail from './pages/Detail/Detail';
import Login from './pages/Login';
import Reg from './pages/Reg';
import Shopcarts from './pages/Shopcarts/Shopcarts';
import Set from './pages/Main/Set';
import Orders from './pages/Shopcarts/orders/Orders';
import Order from './pages/Main/order/Order';
import OrderDetail from './pages/Main/order/OrderDetail';
import Vip from './pages/Main/vip/Vip';

import './App.css';
function App() {
  // const [path, setPath] = useState('')
  // const { pathname } = useLocation()
  // useEffect(() => {
  //   setPath(pathname)
  //   console.log(1);
  // }, [])
  return (
    <>
      {/* <Progress percent={30} position="fixed" /> */}
      <Switch>
        {/* 主页 */}
        <Route path="/home" component={Home}></Route>
        {/* 分类 */}
        <Route path="/category" component={Category}></Route>
        {/* 我的 */}
        <Route path="/main" component={Main}></Route>
        {/* 登录 */}
        <Route path="/login" component={Login}></Route>
        {/* 注册 */}
        <Route path="/reg" component={Reg}></Route>
        {/* 购物车 */}
        <Route path="/shopcarts" component={Shopcarts}></Route>
        {/* 商品详情 */}
        <Route path="/detail" component={Detail}></Route>
        {/* 设置 */}
        <Route path="/set" component={Set}></Route>
        {/* 填写订单 */}
        <Route path="/order" component={Order}></Route>
        {/* 会员中心 */}
        <Route path="/vip" component={Vip}></Route>
        {/* 订单详情页 */}
        <Route path="/orderdetail" component={OrderDetail}></Route>
        {/* 订单列表页 */}
        <Route path="/orders" component={Orders}></Route>
        {/* 重定向 */}
        <Redirect to="/home" from="/"></Redirect>
      </Switch>
      <Footer></Footer>
    </>
  );
}

export default App;
