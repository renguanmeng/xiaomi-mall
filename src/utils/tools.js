// export const base_url = 'http://127.0.0.1:3009'
export const base_url = 'http://api.cat-shop.penkuoer.com'


// 图片路径处理
export const dalimageUrl = (str) => {
  if (str) {
    if (str.startsWith('http')) {
      return str
    } else {
      return base_url + str
    }
  } else {  //数据苦里面没有图片地址
    return 'https://dss2.bdstatic.com/6Ot1bjeh1BF3odCf/it/u=412255206,2110567950&fm=218&app=92&f=PNG?w=121&h=75&s=BBC34B90504342E84022F543030030F8'
  }
}

export const upload_url = base_url + "/api/v1/common/file_upload";