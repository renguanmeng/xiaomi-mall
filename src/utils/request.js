import axios from 'axios'
// import { message } from 'antd';
import { base_url } from './tools'
import { getToken } from './auth'

const instance = axios.create({
  baseURL: base_url,
  timeout: 5000
})

// 全局请求拦截
instance.interceptors.request.use(function (config) {
  if (getToken()) {
    config.headers.authorization = 'Bearer ' + getToken()
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});
// 全局响应拦截
instance.interceptors.response.use(function (response) {

  return response.data;
}, function (error) {
  // message.error('出错了')
  console.log(error.response)
  if (error.response && error.response.status === 401) {
    window.location.href = "/#/login"
  }
  return Promise.reject(error);
});
/**
 * params{1}  url地址
 * params{2} 请求的数据
 * get请求封装
*/

export const get = (url, params) => instance.get(url, { params })
export const post = (url, data) => instance.post(url, data)
export const put = (url, data) => instance.put(url, data)

export const del = (url) => instance.delete(url)

