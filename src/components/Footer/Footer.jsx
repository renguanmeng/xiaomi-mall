import React, { useState, useEffect } from 'react';
import { TabBar } from 'antd-mobile'
import { useHistory, useLocation } from 'react-router-dom'




function Footer() {
    const { pathname } = useLocation()
    const [selectedTab, setSelectedTab] = useState(pathname)
    const [hidden, setHidden] = useState(false)
    const { push } = useHistory()
    useEffect(() => {
        if (pathname === '/home' || pathname === '/category' || pathname === '/main') {
            setHidden(false)
        } else {
            setHidden(true)
        }
        setSelectedTab(pathname)
    }, [pathname])

    return (
        <div style={{ position: 'fixed', width: '100%', bottom: 0, zIndex: 10 }}>
            <TabBar
                unselectedTintColor="#949494"
                tintColor="#33A3F4"
                barTintColor="white"
                hidden={hidden}
            >
                <TabBar.Item
                    title="主页"
                    key="Life"
                    icon={<div style={{
                        width: '22px',
                        height: '22px',
                        background: 'url(https://zos.alipayobjects.com/rmsportal/sifuoDUQdAFKAVcFGROC.svg) center center /  21px 21px no-repeat'
                    }}
                    />
                    }
                    selectedIcon={<div style={{
                        width: '22px',
                        height: '22px',
                        background: 'url(https://zos.alipayobjects.com/rmsportal/iSrlOTqrKddqbOmlvUfq.svg) center center /  21px 21px no-repeat'
                    }}
                    />
                    }
                    selected={selectedTab === '/home' || selectedTab === '/'}
                    onPress={() => {
                        push('/home')
                        setSelectedTab('/home')
                    }}
                    data-seed="logId"
                >

                </TabBar.Item>
                <TabBar.Item
                    icon={
                        <div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(https://gw.alipayobjects.com/zos/rmsportal/BTSsmHkPsQSPTktcXyTV.svg) center center /  21px 21px no-repeat'
                        }}
                        />
                    }
                    selectedIcon={
                        <div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(https://gw.alipayobjects.com/zos/rmsportal/ekLecvKBnRazVLXbWOnE.svg) center center /  21px 21px no-repeat'
                        }}
                        />
                    }
                    title="分类"
                    key="Koubei"
                    selected={selectedTab === '/category'}
                    onPress={() => {
                        push('/category')
                        setSelectedTab('/category')
                    }}
                    data-seed="logId1"
                >
                </TabBar.Item>

                <TabBar.Item
                    icon={{ uri: 'https://zos.alipayobjects.com/rmsportal/asJMfBrNqpMMlVpeInPQ.svg' }}
                    selectedIcon={{ uri: 'https://zos.alipayobjects.com/rmsportal/gjpzzcrPMkhfEqgbYvmN.svg' }}
                    title="我的"
                    key="my"
                    selected={selectedTab === '/main'}
                    onPress={() => {
                        push('/main')
                        setSelectedTab('/main')
                    }}
                >
                </TabBar.Item>
            </TabBar>
        </div>
    );
}

export default Footer;
