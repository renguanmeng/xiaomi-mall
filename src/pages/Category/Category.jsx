import React, { useEffect, useState } from 'react'
import { Tabs, WhiteSpace, WingBlank, Tag, Button, NavBar, Icon } from 'antd-mobile';
import { getProductCategoryAPI, getProductAPI } from '../../service/products'

function Category() {
    const [list, setList] = useState([])
    const getlist = (page) => {
        getProductAPI({ page }).then(res => {
            console.log(res);
            setList([...res.products])
        })
    }
    useEffect(() => {
        getlist(1)
    }, [])

    const tabs = [
        { title: '小米', key: 't1', page: 1 },
        { title: '华为', key: 't2', page: 2 },
        { title: '魅族', key: 't3', page: 3 },
        { title: '迪奥', key: 't4', page: 4 },
        { title: '电脑', key: 't5', page: 5 },
        { title: '苹果', key: 't6', page: 6 },
    ];
    const TabClick = (tab) => {
        console.log(tab);
        getlist({ page: tab.page })
    }
    return (
        <div style={{ height: "calc(100% - 50px)" }}>
            <div style={{ position: "fixed", top: 0, zIndex: 1000, width: '100%' }}>
                <NavBar
                    mode="light"
                >商品分类</NavBar></div>
            <div style={{ height: "100%" }}>
                <Tabs tabs={tabs}
                    style={{ height: "100%" }}
                    initialPage={'t2'}
                    tabBarPosition="left"
                    tabDirection="vertical"
                    onTabClick={TabClick}
                >
                    <div>
                        <WingBlank>
                            <WhiteSpace size="lg" />
                            {
                                list.map(item => (
                                    <Button key={item._id} type="ghost" inline size="small" style={{ marginRight: '4px', color: '#000' }}>{item.name}</Button>
                                ))
                            }
                        </WingBlank>
                    </div>
                </Tabs>
            </div>
        </div>
    )
}

export default Category
