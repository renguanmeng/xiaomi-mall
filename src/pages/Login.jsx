import React, { useState, useEffect } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { List, InputItem, Toast } from 'antd-mobile';
import { loginAPI } from '../service/auth'
import { getToken, setToken } from '../utils/auth'

function Login() {
    const { push, goBack } = useHistory()
    const [userName, setUsername] = useState("admin")
    const [password, setPassword] = useState("admin")
    useEffect(() => {
        if (getToken()) {
            goBack()
        }
    }, [])
    const handleClick = () => {
        loginAPI({ userName, password }).then(res => {
            console.log(res);
            if (res.code === "success") {
                setUsername('')
                setPassword('')
                setToken(res.token)
                Toast.success('登陆成功', 1);
                setTimeout(() => {
                    push('/main')
                }, 1000);
            } else {
                Toast.fail(res.message, 1);
            }


        })
    }
    return (
        <>
            <List style={{ zIndex: 1, position: 'relative' }} renderHeader={<h2 style={{ color: 'black', textAlign: 'center', margin: 0 }}>登录页</h2>}>
                <InputItem
                    maxLength={10}
                    placeholder="auto focus"
                    value={userName}
                    onChange={(e) => {
                        setUsername(e)
                    }}
                >用户名</InputItem>
                <InputItem
                    placeholder="click the button below to focus"
                    value={password}
                    onChange={(e) => setPassword(e)}
                >密码</InputItem>
                <List.Item>
                    <div
                        style={{ width: '100%', color: '#108ee9', textAlign: 'center' }}
                        onClick={handleClick}
                    >
                        登录
            </div>
                </List.Item>
            </List>
            <Link to="/reg">还没有账号，立即注册</Link>
        </>
    )
}

export default Login
