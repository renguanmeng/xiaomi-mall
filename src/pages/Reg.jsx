import React, { useState, useEffect } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { List, InputItem, WhiteSpace, Toast } from 'antd-mobile';
import axios from 'axios'
function Reg() {
    const { push } = useHistory()
    const [userName, setUserName] = useState("admin")
    const [password, setPassword] = useState("admin")

    const handleClick = () => {
        console.log(userName, password);
        axios.post('http://localhost:3009/api/v1/auth/reg', { userName: userName, password: password }).then(res => {
            console.log(res.data);
            if (res.data.code === 'success') {
                setUserName('')
                setPassword('')
                Toast.success('注册成功', 1);
                setTimeout(() => {
                    push('/login')
                }, 1000);
            } else {
                Toast.fail(res.data.message, 1);
            }


        })
    }
    return (
        <>
            <List style={{ zIndex: 1, position: 'relative' }} renderHeader={<h2 style={{ color: 'black', textAlign: 'center', margin: 0 }}>注册页</h2>}>
                <InputItem
                    maxLength={6}
                    placeholder="auto focus"
                    value={userName}
                    onChange={(e) => {
                        setUserName(e)
                    }}
                >用户名</InputItem>
                <InputItem
                    placeholder="click the button below to focus"
                    value={password}
                    onChange={(e) => setPassword(e)}
                >密码</InputItem>
                <List.Item>
                    <div
                        style={{ width: '100%', color: '#108ee9', textAlign: 'center' }}
                        onClick={handleClick}
                    >
                        注册
            </div>
                </List.Item>
            </List>
            <Link to="/login">已有账号，直接去登录</Link>
        </>
    )
}

export default Reg
