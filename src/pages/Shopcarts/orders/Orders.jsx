import React, { Component } from 'react'
import { createForm } from 'rc-form';
import { Picker, List, WhiteSpace, NavBar, Icon, InputItem, Button, TextareaItem, Toast } from 'antd-mobile';
import { district, provinceLite } from 'antd-mobile-demo-data';
import arrayTreeFilter from 'array-tree-filter';
import { ordersAPI } from '../../../service/products'
class Orders extends Component {
    state = {

        visible: false,
        receiver: '',
        regions: '',
        address: '',
        orderDetails: [],
    };
    componentDidMount() {
        console.log(this.props.location.state);
        const product = this.props.location.state
        this.setState(
            {
                regions: this.getSel(),
                orderDetails: [{
                    quantity: product.quantity,
                    product: product.product._id,
                    price: product.product.price
                }]
            }
        )

    }
    onChange = (e) => {

        this.setState({ receiver: e.target.value })
    }
    onChange2 = (e) => {
        // const value = this.customFocusInst.state.value
        this.setState({ address: e.target.value })
    }
    getSel = () => {
        const value = this.state.pickerValue;
        if (!value) {
            return '';
        }
        const treeChildren = arrayTreeFilter(district, (c, level) => c.value === value[level]);
        const regions = treeChildren.map(v => v.label).join(',')
        return regions
    }

    orders = () => {
        console.log(this.state);
        if (!(this.state.receiver && this.state.regions && this.state.address)) {
            Toast.fail('请填写完整信息', 1, () => {
            });
            return
        }
        ordersAPI({
            receiver: this.state.receiver,
            regions: this.state.regions,
            address: this.state.address,
            orderDetails: this.state.orderDetails
        }).then(res => {
            console.log(res);
            Toast.loading('进入订单页', 1, () => {
                this.props.history.push({
                    pathname: '/order'
                })
            });
        }
        )
    }
    render() {
        const { push, goBack } = this.props.history

        // const { getFieldProps } = this.props.form;
        return (
            <div>
                <div style={{ position: "fixed", top: 0, zIndex: 1000, width: '100%' }}>
                    <NavBar
                        mode="light"
                        icon={<Icon type="left" />}
                        onLeftClick={() => goBack()}
                    >订单填写提交</NavBar></div>
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <WhiteSpace size="lg" />
                <List >
                    <InputItem
                        ref={el => this.labelFocusInst = el}
                        onInput={(e) => this.onChange(e)}
                        value={this.state.receiver}
                    ><div onClick={() => this.labelFocusInst.focus()}>收货人</div></InputItem>
                </List>
                <WhiteSpace />
                <Picker
                    visible={this.state.visible}
                    data={district}
                    value={this.state.pickerValue}
                    onChange={v => {
                        if (!v) {
                            return '';
                        }
                        const treeChildren = arrayTreeFilter(district, (c, level) => c.value === v[level]);
                        const regions = treeChildren.map(v => v.label).join(',')
                        this.setState({ pickerValue: v, regions })
                    }}
                    onOk={() => this.setState({ visible: false })}
                    onDismiss={() => this.setState({ visible: false })}
                >
                    <List.Item extra={this.getSel()} onClick={() => this.setState({ visible: true })}>
                        选择城市
                    </List.Item>

                </Picker>
                <WhiteSpace />
                <TextareaItem
                    title="收货地址"
                    data-seed="logId"
                    autoHeight
                    value={this.state.address}
                    onInput={(e) => this.onChange2(e)}
                />
                <WhiteSpace />
                <Button type="warning" onClick={this.orders}>提交订单</Button><WhiteSpace />
            </div>
        )
    }
}
const TestWrapper = createForm()(Orders);
export default TestWrapper
