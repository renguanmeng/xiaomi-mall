import React, { useEffect, useState } from 'react'
import { useLocation, useHistory } from 'react-router-dom'
import { Icon, Button, Card, WingBlank, WhiteSpace, NavBar, Toast, Checkbox } from 'antd-mobile'
import { MinusOutlined, PlusOutlined, CloseCircleOutlined } from '@ant-design/icons'
import { getshopCartsAPI, shopAddCartsAPI, delProductCatsAPI } from '../../service/products'



function Shopcarts() {
    const [data, setData] = useState([])
    const [status, setStatus] = useState(false)
    const [thisStatus, setThisStatus] = useState([])
    const [show, setShow] = useState('block')
    const { push, goBack } = useHistory()
    const [val, setVal] = useState([])
    const [sprice, setSprice] = useState([])
    const CheckboxItem = Checkbox.CheckboxItem;
    const whether = (e) => {
        setStatus(e.target.checked);
        const arr = thisStatus
        const brr = arr.map(item => e.target.checked)
        setThisStatus([...brr])
    }
    const onChange = (e, index) => {
        const arr = thisStatus
        // console.log(e.target.checked);
        arr.splice(index, 1, e.target.checked)
        setThisStatus([...arr]);
        console.log(arr);
        console.log(arr.findIndex(item => item === false));
        // if (arr.findIndex(item => item === false) === -1) {
        //     setStatus(true)
        // } else {
        //     setStatus(false)
        // }
    }
    useEffect(() => {
        getlist()


    }, [])
    useEffect(() => {
        let price = 0
        data.forEach((item, index) => {
            price += Number(val[index]) * item.product.price
        })
        setSprice(price)
        if (val.length > 0) {
            setShow('none')
        } else {
            setShow('block')
        }
        const arr = thisStatus
        // console.log(arr);
        if (arr.findIndex(item => item === false) === -1) {
            setStatus(true)
        } else {
            setStatus(false)
        }
    }, [val, thisStatus])
    const getlist = () => {
        getshopCartsAPI().then(res => {
            setData([...res])
            const newNum = res.map(item => {
                return item.quantity
            })
            const arr = thisStatus
            if (thisStatus.length === 0) {
                res.forEach(() => arr.push(false))
            }
            setVal(newNum)
            setThisStatus([...arr])
        })
    }


    return (
        <div>
            <div style={{ position: "fixed", top: 0, zIndex: 1000, width: '100%' }}>
                <NavBar
                    mode="light"
                    icon={<Icon type="left" />}
                    onLeftClick={() => goBack()}
                >购物车</NavBar></div>
            <div style={{ marginTop: 45 }}>
                <WingBlank size="lg" style={{ display: show }}>
                    <WhiteSpace size="lg" />
                    <Card>

                        <Card.Body>
                            <div>购物车空空如也，赶快去购物......</div>
                        </Card.Body>
                        <Card.Footer extra={<Button type="primary" onClick={() => { push('/') }} inline size="small" style={{ marginRight: '4px' }}>去主页</Button>} />
                    </Card>
                    <WhiteSpace size="lg" />
                </WingBlank>
            </div>


            {
                data.map((item, index) => (
                    <div key={item._id} style={{ overflow: 'hidden' }}>

                        <Card>
                            <Card.Header
                                title={<div style={{ textIndent: 70 }}>
                                    {'商品' + (index + 1)}

                                </div>}

                                extra={<span onClick={() => {
                                    delProductCatsAPI(item._id).then(res => {
                                        console.log(item._id);
                                        console.log(res);
                                        getlist()
                                    })
                                }}><CloseCircleOutlined /></span>}
                            />
                            <Card.Body>
                                <CheckboxItem style={{ position: 'absolute', top: -45 }} checked={thisStatus[index]} onChange={(e) => onChange(e, index)}>
                                </CheckboxItem>
                                <div style={{ overflow: 'hidden' }}>
                                    <img style={{ width: 100, float: "left" }} src={item.product.coverImg} alt="" />
                                    <div>{item.product.name}</div>
                                    <p style={{ color: "red" }}>
                                        ￥{item.product.price}

                                    </p>

                                </div>

                                <div style={{ width: '100%', textAlign: 'center' }}>
                                    购买数量
                                <button onClick={() => {
                                        // 购物车数量减
                                        if (val[index] > 1) {
                                            const newNum = val
                                            newNum.splice(index, 1, val[index] - 1)
                                            setVal([...newNum])
                                            shopAddCartsAPI({ product: item.product._id, quantity: -1 }).then(res => {
                                            })
                                        } else {
                                            const newNum = val
                                            newNum.splice(index, 1, 1)
                                            setVal([...newNum])
                                        }
                                    }}><MinusOutlined /></button>
                                    <input style={{ width: 30 }} value={val[index]} onChange={(e) => {
                                        if (val[index] >= 0) {
                                            const newNum = [...val]
                                            newNum.splice(index, 1, Number(e.target.value))
                                            setVal([...newNum])
                                            shopAddCartsAPI({ product: item.product._id, quantity: Number(e.target.value) - val[index] })
                                                .then(res => {
                                                    // console.log(res);
                                                })
                                        }

                                    }} type="text" />
                                    <button onClick={() => {
                                        // 购物车数量加
                                        const newNum = val
                                        newNum.splice(index, 1, val[index] + 1)
                                        setVal([...newNum])
                                        shopAddCartsAPI({ product: item.product._id, quantity: 1 }).then(res => {
                                        })
                                    }}><PlusOutlined /></button>
                                </div>
                            </Card.Body>
                            <Card.Footer
                                content={"总价格:￥" + (Number(val[index]) * item.product.price)}
                                extra={<div>
                                    <Button size="small" type="warning" onClick={() => {
                                        Toast.loading('进入订单', 1, () => {
                                            push({
                                                pathname: '/orders',
                                                state: item
                                            })
                                        });
                                    }}>立即购买</Button>
                                    <WhiteSpace />
                                </div>} />
                        </Card>
                        <WhiteSpace size="lg" />

                    </div>
                ))
            }
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <div style={{ position: 'fixed', width: '100%', background: '#fff', bottom: 0, fontSize: 20, zIndex: 11 }}>
                <CheckboxItem checked={status} onChange={(e) => whether(e)}>
                    全选
</CheckboxItem>
                <div style={{ float: 'left', width: '50%', lineHeight: 1 }}>
                    <span>总价格:<span style={{ color: 'red' }}>￥{sprice}</span></span>
                </div>
                <div style={{ float: 'right', width: '50%' }}>
                    <Button size="small" type="warning" style={{ fontSize: 20 }} onClick={() => {
                        Toast.success('购买成功', 1, () => {
                        });
                    }}>立即购买</Button>
                </div>
            </div>
        </div>
    )
}

export default Shopcarts
