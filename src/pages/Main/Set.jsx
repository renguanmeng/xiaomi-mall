import React, { useEffect, useState } from 'react'
import { useLocation, useHistory } from 'react-router-dom'
import { WhiteSpace, Button, NavBar, Icon, WingBlank, Toast } from 'antd-mobile';
import { getToken, removeToken } from '../../utils/auth'

function Set() {
    const { goBack, push } = useHistory()
    const [data, setData] = useState('立即登录')
    useEffect(() => {
        if (getToken()) {
            setData('退出登录')
        } else {
            setData('立即登录')
        }

    }, [])
    return (
        <div>
            <NavBar
                mode="light"
                icon={<Icon type="left" />}
                onLeftClick={() => goBack()}
            >设置</NavBar>
            <WhiteSpace size="lg" />
            <WingBlank> <Button type="warning" onClick={() => {
                if (getToken()) {
                    removeToken()
                    Toast.loading('Loading...', 1, () => {
                        Toast.loading('已退出,即将进入主页', 1, () => {
                            setData('立即登录')
                            push('/')
                        });
                    });


                } else {
                    Toast.loading('Loading...', 1, () => {
                        push('/login')
                    });

                }

            }}>{data}</Button></WingBlank>
            <WhiteSpace />
        </div>
    )
}

export default Set
