import React, { useEffect, useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { Card, WingBlank, WhiteSpace, Button, NavBar, Icon } from 'antd-mobile';
import { getordersDetailAPI } from '../../../service/products'


function OrderDetail() {
    const { state } = useLocation()
    const { goBack, push } = useHistory()
    const [list, setList] = useState('')
    useEffect(() => {
        getordersDetailAPI(state.id).then(res => {
            console.log(res.details);
            setList(res.details[0])
        })
    }, [])
    return (
        <div>
            <div style={{ position: "fixed", top: 0, zIndex: 1000, width: '100%' }}>
                <NavBar
                    mode="light"
                    icon={<Icon type="left" />}
                    onLeftClick={() => goBack()}
                >订单详情</NavBar></div>
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <Card>
                <Card.Header
                    title={list === '' ? '' : list.product.name.split(" ")[0]}
                    thumb="https://gw.alipayobjects.com/zos/rmsportal/MRhHctKOineMbKAZslML.jpg"
                    extra={<span style={{ color: 'red' }}>￥{list === '' ? '' : list.product.price}</span>}
                />
                <Card.Body>
                    <img style={{ float: 'left', width: 100, height: 100 }} src={list === '' ? '' : list.product.coverImg} alt="" />
                    <div>{list === '' ? '' : list.product.descriptions}</div>
                    <p>配送至：{state.address}</p>
                </Card.Body>
                <Card.Footer
                    content={list === '' ? "购买数量：" : "购买数量：" + list.quantity}
                    extra={<Button onClick={() => {

                    }} type="warning" inline size="small" style={{ marginRight: '4px' }}>删除订单</Button>} />
            </Card>
        </div>
    )
}

export default OrderDetail
