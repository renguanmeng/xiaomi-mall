import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Card, WingBlank, WhiteSpace, Button, NavBar, Icon } from 'antd-mobile';
import { getordersAPI } from '../../../service/products'
function Order() {
    const { push, goBack } = useHistory()
    const [orderlist, setOrderlist] = useState([])
    useEffect(() => {
        getordersAPI().then(res => {
            console.log(res.orders);
            setOrderlist([...res.orders])
        })
    }, [])

    return (
        <div>
            <div style={{ position: "fixed", top: 0, zIndex: 1000, width: '100%' }}>
                <NavBar
                    mode="light"
                    icon={<Icon type="left" />}
                    onLeftClick={() => goBack()}
                >订单列表</NavBar></div>
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            {
                orderlist.map((item, index) => (
                    <div key={item._id}>
                        <Card>
                            <Card.Header
                                title={"订单" + index}
                                thumb="https://gw.alipayobjects.com/zos/rmsportal/MRhHctKOineMbKAZslML.jpg"
                                extra={<span style={{ color: 'red' }}>￥{item.price}</span>}
                            />
                            <Card.Body>
                                <div>地址：{item.regions + item.address}</div>
                            </Card.Body>
                            <Card.Footer
                                content={"收货人：" + item.receiver}
                                extra={<Button onClick={() => {
                                    push(
                                        {
                                            pathname: '/orderdetail',
                                            state: { id: item._id, address: item.regions + item.address }
                                        }
                                    )
                                }} type="ghost" inline size="small" style={{ marginRight: '4px' }}>详情</Button>} />
                        </Card>
                        <WhiteSpace size="lg" />
                    </div>
                ))
            }

        </div>
    )
}

export default Order
