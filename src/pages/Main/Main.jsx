import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { WalletOutlined, CarFilled, ToolFilled, CrownFilled, SettingFilled, GiftFilled, BankFilled, HeartFilled, GoogleSquareFilled } from '@ant-design/icons'
import { Card, WhiteSpace, List, Grid } from 'antd-mobile';
import { getToken } from '../../utils/auth'
const Item = List.Item;
const data = [
    {
        icon: (<WalletOutlined />),
        text: `待付款`,
    },
    {
        icon: (<CarFilled />),
        text: `待收货`,
    },
    {
        icon: (<ToolFilled />),
        text: `退换修`,
    },
]
function Main() {
    const [name, setName] = useState("任关蒙")
    const { push } = useHistory()

    useEffect(() => {
        if (getToken()) {
            setName("任关蒙")
        }
    }, [])
    return (
        <div style={{ paddingBottom: 50 }}>
            <Card>
                <Card.Header
                    style={{ background: "url(https://m.mi.com/static/img/bg.63c8e19851.png) center 0 #f37d0f", height: 75 }}
                    title={<span onClick={() => {
                        // console.log(getToken());
                        if (!getToken()) {
                            push('/login')
                        } else {
                            // push('/login')
                        }
                    }} style={{ color: '#fff', zIndex: 9 }}>{getToken() ? name : "登录/注册"}</span>}

                    thumb="https://gw.alipayobjects.com/zos/rmsportal/MRhHctKOineMbKAZslML.jpg"
                />
                <Card.Body>
                    <List className="my-list">
                        <Item
                            arrow="horizontal"
                            multipleLine
                            onClick={() => {
                                console.log(111);
                                push('/order')
                            }}
                            extra="全部订单"
                        >
                            我的订单
                        </Item>
                    </List>
                </Card.Body>
                <Card.Body >
                    <Grid itemStyle={{ height: 70 }} columnNum={3} data={data} hasLine={false} />
                </Card.Body>
            </Card>
            <WhiteSpace size="lg" />
            <List className="my-list">
                <Item
                    arrow="horizontal"
                    thumb={<CrownFilled />}
                    multipleLine
                    onClick={() => {
                        push('/vip')
                    }}
                >
                    会员中心
                </Item>
            </List>
            <List className="my-list">
                <Item
                    arrow="horizontal"
                    thumb={<WalletOutlined />}
                    multipleLine
                    onClick={() => { }}
                >
                    我的优惠
                </Item>
            </List>
            <WhiteSpace size="lg" />
            <List className="my-list">
                <Item
                    arrow="horizontal"
                    thumb={<HeartFilled />}
                    multipleLine
                    onClick={() => { }}
                >
                    服务中心
                </Item>
            </List>
            <List className="my-list">
                <Item
                    arrow="horizontal"
                    thumb={<BankFilled />}
                    multipleLine
                    onClick={() => { }}
                >
                    小米之家
                </Item>
            </List>
            <WhiteSpace size="lg" />
            <List className="my-list">
                <Item
                    arrow="horizontal"
                    thumb={<GoogleSquareFilled />}
                    multipleLine
                    onClick={() => { }}
                >
                    我的F码
                </Item>
            </List>
            <List className="my-list">
                <Item
                    arrow="horizontal"
                    thumb={<GiftFilled />}
                    multipleLine
                    onClick={() => { }}
                >
                    礼物码兑换
                </Item>
            </List>
            <WhiteSpace size="lg" />
            <List className="my-list">
                <Item
                    arrow="horizontal"
                    thumb={<SettingFilled />}
                    multipleLine
                    onClick={() => { push('/set') }}
                >
                    设置
                </Item>
            </List>
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
        </div >

    )
}

export default Main
