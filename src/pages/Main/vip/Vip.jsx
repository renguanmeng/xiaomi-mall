import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Modal, WhiteSpace, Toast, Grid, NavBar, Icon } from 'antd-mobile';
import { Image } from 'antd';
import axios from 'axios'
import './Vip.css'
const prompt = Modal.prompt;
function Vip() {
    const [showVip, setShowVip] = useState(false)
    const [swiperImg, setSwiperImg] = useState([])
    const { goBack, push } = useHistory()
    const getlist = (num) => {
        axios.get('https://gank.io/api/v2/data/category/Girl/type/Girl/page/1/count/' + num).then(res => {
            console.log(res);
            const ImgList = res.data.data
            setSwiperImg([...ImgList])
        })
    }
    useEffect(() => {
        setShowVip(false)
        prompt(
            'Password',
            '请输入vip暗号',
            password => {
                if (password === "0000") {
                    Toast.success('输入正确！', 1, () => {
                        setShowVip(true)
                        getlist(100)
                    });
                } else {
                    Toast.fail('输入错误！即将退回原页面', 1, () => {
                        goBack()
                    });

                }
            },
            'secure-text',
        )
    }, [])
    return (
        <div>
            <div style={{ position: "fixed", top: 0, zIndex: 1, width: '100%' }}>
                <NavBar
                    mode="light"
                    icon={<Icon type="left" />}
                    onLeftClick={() => goBack()}
                >会员中心</NavBar></div>

            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <div style={{ display: showVip ? "block" : "none" }} >

                <Grid
                    square={false}
                    data={swiperImg}
                    columnNum={2}

                    renderItem={dataItem => (
                        <div style={{ padding: '12.5px' }}>
                            <Image
                                src={dataItem.url}
                                style={{ width: '100%' }}
                                alt=""
                            />
                            <div style={{ color: '#888', fontSize: '14px', marginTop: '12px' }}>
                                <span>{dataItem.desc}---{dataItem.title}</span>
                            </div>
                        </div>
                    )}
                />
            </div>
        </div>
    )
}

export default Vip
