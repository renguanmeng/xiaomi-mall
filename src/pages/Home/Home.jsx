import React from 'react'
import Header from './Header'
import Recommend from './recommend/Recommend';
import Phone from './phone/Phone';
import { Tabs, WhiteSpace } from 'antd-mobile';
import './Home.css'


const tabs2 = [
    { title: '推荐', sub: '1' },
    { title: '手机', sub: '2' },
    { title: '智能', sub: '3' },
    { title: '电视', sub: '4' },
    { title: '家电', sub: '5' },
    { title: '笔记本', sub: '6' },
];
function Home() {
    return (
        <div >
            <Header />
            <Tabs tabs={tabs2}
                initialPage={0}
                swipeable={false}
                tabBarPosition="top"
                renderTab={tab => <span>{tab.title}</span>}
            >
                <Recommend></Recommend>
                {/* <div><Recommend></Recommend></div> */}
                <div><Phone></Phone></div>
                {/* <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '150px', backgroundColor: '#fff' }}>
                    Content of first tab
      </div>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '150px', backgroundColor: '#fff' }}>
                    Content of second tab
      </div>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '150px', backgroundColor: '#fff' }}>
                    Content of third tab
      </div> */}


            </Tabs>
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />
            <WhiteSpace size="lg" />

        </div>
    )
}

export default Home
