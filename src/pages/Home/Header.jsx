import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { NavBar, Icon, SearchBar, Popover, Toast } from 'antd-mobile';
import { ShoppingCartOutlined, UserOutlined, UsergroupDeleteOutlined } from '@ant-design/icons'
import { getToken } from '../../utils/auth'

function Header() {
    const { push } = useHistory()
    const Item = Popover.Item;

    const myImg = src => <img src={`https://gw.alipayobjects.com/zos/rmsportal/${src}.svg`} className="am-icon am-icon-xs" alt="" />;
    const [visible, setVisible] = useState(false)
    const [name, setName] = useState('登录')
    useEffect(() => {
        if (getToken()) {
            setName('已登陆')
        }
    }, [])
    const onSelect = (opt) => {
        console.log(opt.props.value);
        if (getToken()) {
            if (opt.props.value === "/shopcarts") {
                push(opt.props.value)
            }
            if (opt.props.value === "/login") {
                // push(opt.props.value)
            }
            if (opt.props.value === "/main") {
                push(opt.props.value)
            }
        } else {
            if (opt.props.value === "/shopcarts") {
                Toast.fail('您还未登录，无法进入购物车', 1, () => {
                });
            }
            if (opt.props.value === "/login") {
                Toast.loading('Loading...', 1, () => {
                    push(opt.props.value)
                });
            }
            if (opt.props.value === "/main") {
                push(opt.props.value)
            }

        }

        setVisible(false)
        // setSelected(opt.props.value)


    };
    const handleVisibleChange = (visible) => {
        setVisible(visible)
    };

    return (
        <div style={{ position: "fixed", top: 0, zIndex: 10, width: '100%' }}>
            <NavBar
                mode="light"
                icon={<Icon type="left" />}
                onLeftClick={() => console.log('onLeftClick')}
                rightContent={
                    <Popover mask
                        overlayClassName="fortest"
                        overlayStyle={{ color: 'currentColor' }}
                        visible={visible}
                        overlay={[
                            (<Item key="4" value="/login" icon={<UserOutlined style={{ fontSize: 22, textIndent: -4 }} />} data-seed="logId">{name}</Item>),
                            (<Item key="5" value="/shopcarts" icon={<ShoppingCartOutlined style={{ fontSize: 22, textIndent: -4 }} />} style={{ whiteSpace: 'nowrap' }}>购物车</Item>),
                            (<Item key="6" value="/main" icon={<UsergroupDeleteOutlined style={{ fontSize: 22, textIndent: -4 }} />}>
                                <span style={{ marginRight: 5 }}>我的</span>
                            </Item>),
                        ]}
                        align={{
                            overflow: { adjustY: 0, adjustX: 0 },
                            offset: [0, 0],
                        }}
                        onVisibleChange={handleVisibleChange}
                        onSelect={onSelect}
                    >
                        <div style={{
                            height: '100%',
                            padding: '0 15px 0 0',
                            marginRight: '-15px',
                            display: 'flex',
                            alignItems: 'center',
                        }}
                        >
                            <Icon type="ellipsis" />
                        </div>
                    </Popover>
                }

            >
                <div>
                    <SearchBar placeholder="Search" maxLength={8} style={{ background: '#fff', marginTop: -1, width: 280, height: '100%' }} />
                    {/* <WhiteSpace /> */}

                </div>
            </NavBar>

        </div>
    )
}

export default Header
