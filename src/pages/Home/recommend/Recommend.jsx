import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Carousel, Grid } from 'antd-mobile';
import axios from 'axios'
import { getProductAPI } from '../../../service/products'

const Recommend = () => {
    const [data, setData] = useState([])
    const [data1, setData1] = useState([])
    const [swiperImg, setSwiperImg] = useState([])
    const { push } = useHistory()
    // const data1 = Array.from(new Array(10)).map(() => ({
    //     icon: 'https://gw.alipayobjects.com/zos/rmsportal/WXoqXTHrSnRcUwEaQgXJ.png',
    // }));
    const getlist = (num) => {
        getProductAPI({ per: num }).then(res => {
            console.log(res);

            setData1([...res.products])
        })
    }
    useEffect(() => {
        getProductAPI({ per: 8 }).then(res => {
            console.log(res);
            const newData = res.products.map((item, index) => {
                return {
                    icon: item.coverImg,
                    text: 'name' + index,
                }
            })
            setData(newData)
        })
        axios.get('https://gank.io/api/v2/categories/GanHuo').then(res => {
            console.log(res);
            const ImgList = res.data.data.map(item => item.coverImageUrl)
            setSwiperImg([...ImgList])
        })
        getlist(36)
        console.log(swiperImg);
    }, [])

    return (
        <>
            <Carousel autoplay={true} infinite={true}>
                {swiperImg.map((item, index) => (<img key={index} style={{ height: 200 }} src={item} alt=""></img>))}
                {/* <img src="https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2907773030,2569079192&fm=26&gp=0.jpg" alt="" />
                <img src="https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=4132782348,1291910994&fm=26&gp=0.jpg" alt="" />
                <img src="https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2619042694,1309674893&fm=26&gp=0.jpg" alt="" /> */}
            </Carousel>
            <div>
                <Grid data={data} hasLine={false} />
            </div>
            <div className="sub-title" style={{ height: 30 }}>更多推荐</div>
            <Grid data={data1}
                columnNum={2}
                itemStyle={{ height: 300 }}
                renderItem={dataItem => (
                    <div style={{ padding: '12.5px' }} onClick={() => {
                        push({ pathname: '/detail', state: dataItem })
                    }}>
                        <img src={dataItem.coverImg} style={{ width: '150px', height: '150px' }} alt="" />
                        <div style={{ color: '#888', fontSize: '14px', marginTop: '12px' }}>
                            <span>{dataItem.name}</span>
                        </div>
                    </div>
                )
                }
            />

        </>
    )
}

export default Recommend
