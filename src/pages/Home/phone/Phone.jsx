import React, { useState, useEffect } from 'react'
import { Grid } from 'antd-mobile';
import axios from 'axios'
import { getProductAPI } from '../../../service/products'
const Phone = () => {
    const [data, setData] = useState([])
    const [data1, setData1] = useState([])

    const getlist = (num) => {
        getProductAPI({ per: num }).then(res => {
            // console.log(res.data);

            setData1([...res.products])
        })
    }
    useEffect(() => {
        getProductAPI({ per: 8 }).then(res => {
            // console.log(res.data);
            const newData = res.products.map((item, index) => {
                return {
                    icon: item.coverImg,
                    text: 'name' + index,
                }
            })
            setData(newData)
        })
        getlist(36)
    }, [])
    return (
        <>
            <div className="sub-title" style={{ height: 30 }}>更多推荐</div>
            <Grid

                data={data1}
                columnNum={2}
                renderItem={dataItem => (
                    <div style={{ padding: '12.5px' }}>
                        <img src={dataItem.coverImg} style={{ width: '150px', height: '150px' }} alt="" />
                        <div style={{ color: '#888', fontSize: '14px', marginTop: '12px' }}>
                            <span>{dataItem.name}</span>
                        </div>
                    </div>
                )}
            />
        </>
    )
}

export default Phone
