import React, { useState } from 'react'
import { useLocation, useHistory } from 'react-router-dom'
import { Icon, Button, List, WhiteSpace, NavBar, Stepper, Toast } from 'antd-mobile'
import { ShoppingCartOutlined } from '@ant-design/icons'
import { shopAddCartsAPI } from '../../service/products'
import './Detail.css'
function Detail() {
    const { state } = useLocation()
    const { push, goBack } = useHistory()
    console.log(state);
    const [val, serVal] = useState(1)

    const onChange = (val) => {
        serVal(val);
    }
    const addcart = (id) => {
        console.log(111);
        shopAddCartsAPI({ product: id, quantity: val }).then(res => {
            console.log(res);
            if (res.code === "success") {
                Toast.success(res.message, 1);
            }
        })
    }
    return (
        <div className="detail">
            <div style={{ position: "fixed", top: 0, zIndex: 10, width: '100%' }}>
                <NavBar
                    mode="light"
                    icon={<Icon type="left" />}
                    onLeftClick={() => goBack()}
                    rightContent={<ShoppingCartOutlined onClick={() => { push('/shopcarts') }} style={{ fontSize: 22, textIndent: -4 }} />}
                >商品详情</NavBar></div>
            <img src={state.coverImg} alt="" />
            <List.Item
                multipleLine
            >
                <p style={{ color: 'red' }}>￥{state.price}</p>
                {state.name}
                <List.Item.Brief>

                    {state.descriptions}

                </List.Item.Brief>
            </List.Item>
            <List className="my-list">
                <List.Item extra={state.quantity}>现有数量</List.Item>
            </List>
            <List.Item
                wrap
                extra={
                    <Stepper
                        style={{ width: '100%', minWidth: '100px' }}
                        showNumber
                        max={10}
                        min={1}
                        value={val}
                        onChange={onChange}
                    />}
            >
                购买数量
        </List.Item>

            <Button type="primary" style={{ zIndex: 11 }} onClick={() => addcart(state._id)}>加入购物车</Button><WhiteSpace />

        </div >
    )
}

export default Detail
