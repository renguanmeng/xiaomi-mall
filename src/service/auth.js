import { post } from '../utils/request'

// 封装登录接口
export const loginAPI = (data) => post('/api/v1/auth/login', data)