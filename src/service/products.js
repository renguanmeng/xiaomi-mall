import { get, post, put, del } from '../utils/request'

// 获取商品列表
export const getProductAPI = (params) => get("/api/v1/products", params);

// 添加商品
export const saveAPI = (data) => post("/api/v1/admin/products", data);

// 根据id修改
export const modyfyAPI = (id, data) =>
  put("/api/v1/admin/products/" + id, data);

// 根据id删除
export const delByIdAPI = (id) => del("/api/v1/admin/products/" + id);

// 加入购物车
export const shopAddCartsAPI = (data) => post("/api/v1/shop_carts", data);

// 查看购物车
export const getshopCartsAPI = (params) => get("/api/v1/shop_carts", params);

// 商品分类列表
export const getProductCategoryAPI = (params) => get("/api/v1/admin/product_categories", params);

// 删除购物车信息
export const delProductCatsAPI = (id) => del("/api/v1/shop_carts/" + id);

// 订单提交
export const ordersAPI = (data) => post("/api/v1/orders", data);

// 获取订单
export const getordersAPI = (params) => get("/api/v1/orders", params);

// 获取订单详情
export const getordersDetailAPI = (id) => get("/api/v1/orders/" + id);